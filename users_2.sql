-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Фев 19 2019 г., 12:11
-- Версия сервера: 5.6.37
-- Версия PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `debts`
--

-- --------------------------------------------------------

--
-- Структура таблицы `users_2`
--

CREATE TABLE IF NOT EXISTS `users_2` (
  `id` int(11) NOT NULL,
  `to_whom_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `returned` float NOT NULL,
  `id_name` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `users_2`
--

INSERT INTO `users_2` (`id`, `to_whom_name`, `returned`, `id_name`) VALUES
(1, 'Kirill', 230, 1),
(2, 'Jon', 400, 1),
(3, 'Nikolay', 700, 2);
UPDATE `users` SET `returned` = '1200' WHERE `id` = '1';

DELETE FROM `users` WHERE `id` = '1';

SELECT `name`, `how_moch`, `to_whom_name`, `returned`, `currency` FROM `users` LEFT JOIN `users_2` ON `users`.`id` = `users_2`.`id_name`;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `users_2`
--
ALTER TABLE `users_2`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `users_2`
--
ALTER TABLE `users_2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
